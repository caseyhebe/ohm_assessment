"""Update user info

Revision ID: 5aa1b21df3d2
Revises: 00000000
Create Date: 2018-01-09 20:10:03.910359

"""

# revision identifiers, used by Alembic.
revision = '5aa1b21df3d2'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''UPDATE user SET point_balance = 1000 WHERE user_id = 1''')
    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute) VALUES (2, 'LOCATION', 'USA')''')
    op.execute('''UPDATE user SET tier = 'Silver' WHERE user_id = 3''')


def downgrade():
    op.execute('''UPDATE user SET point_balance = 0 WHERE user_id = 1''')
    op.execute('''DELETE FROM rel_user WHERE user_id = 2''')
    op.execute('''UPDATE user SET tier = 'Carbon' WHERE user_id = 3''')
