from ._helpers import *

class RelUserMulti(db.Model):
    __tablename__ = 'rel_user_multi'
    rum_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'))
    rel_lookup = db.Column(db.String(255))
    attribute = db.Column(db.String(255))
    create_transaction_id = db.Column(db.Integer)
    create_time = db.Column(UTCDateTime, server_default=db.func.current_timestamp())

    @classmethod
    def find_phone_number_by_id(cls, id):
        query = '''SELECT attribute FROM rel_user_multi WHERE rel_lookup = 'PHONE' AND user_id = {id}'''.format(id=id)
        numbers = db.engine.execute(query).fetchall()
        phone_numbers = []
        for n in numbers:
            num = str(n)[3:]
            phone_numbers.append(num[:-3])
        return phone_numbers
