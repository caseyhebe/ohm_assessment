from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User, RelUser, RelUserMulti

@app.route('/community', methods=['GET'])
def community():
    # add funcitonality
    users = User.get_most_recent_five_users()
    for user in users:
        user['phone'] = RelUserMulti.find_phone_number_by_id(user['id'])
        user['location'] = RelUser.find_location_by_id(user['id'])
    args = {'users': users}
    return render_template("community.html", **args)
