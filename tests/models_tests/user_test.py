from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_get_email_address(self):
        assert self.chuck.get_email_address() == 'test@test.com'

    def test_get_points_and_dollars(self):
        assert self.chuck.get_points_and_dollars()["points"] == 1000
        assert self.justin.get_points_and_dollars()["points"] == 0
